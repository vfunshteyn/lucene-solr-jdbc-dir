package org.apache.lucene.store;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

public class JDBCDirectory extends BaseDirectory {
  
  /**
   * Physical location of DB files on the file system
   */
  public final static String DB_PATH_PROP = "lucene.jdbcdir.path";
  
  /**
   * JDBC URL for connecting to the DB; vendor-specific 
   */
  public final static String DB_URL_PROP  = "lucene.jdbcdir.url"; 
  
  // 1 connection per DB table - probably not optimal, but alternatives will likely require pulling in external connection-pooling dependency 
  private final Connection dbConn; 
  
  private final static String NAME_COL = "file_name";
  private final static String DATA_COL = "file_data";
  
  private final static String CREATE_TABLE_SQL = "create table if not exists \"%s\" (%s longvarchar primary key, %s blob)";
  private final static String GET_ALL_FILES_SQL = "select %s from \"%s\"";
  
  private final static String GET_BLOB_SQL = "select * from \"%s\" where %s = ?";
  private final static String DELETE_BLOB_SQL = "delete from \"%s\" where %s = ?";
  private final static String INSERT_BLOB_SQL = "insert into \"%s\" values ?, null";
  private final static String UPDATE_BLOB_SQL = "update \"%s\" set %s = ? where %s = ?";
  
  private final Statement createTableStmt;
  
  private final String dbDirPath;
  private final String dbDirJdbcUrl;
  
  /**
   * Corresponds to table name in DB 
   */
  private final String name;
  
  private final ThreadLocal<PreparedStatement> getBlobStmt = new ThreadLocal<PreparedStatement>() {

    @Override
    protected PreparedStatement initialValue() {
      return prepareStatement(String.format(GET_BLOB_SQL, name, NAME_COL));
    }
  };

  private final ThreadLocal<PreparedStatement> delBlobStmt = new ThreadLocal<PreparedStatement>() {

    @Override
    protected PreparedStatement initialValue() { 
      return prepareStatement(String.format(DELETE_BLOB_SQL, name, NAME_COL));
    }
  };
  
  private final ThreadLocal<PreparedStatement> addBlobStmt = new ThreadLocal<PreparedStatement>() {

    @Override
    protected PreparedStatement initialValue() {
      return prepareStatement(String.format(INSERT_BLOB_SQL, name));
    }
  };
  
  private final ThreadLocal<PreparedStatement> updateBlobStmt = new ThreadLocal<PreparedStatement>() {

    @Override
    protected PreparedStatement initialValue() {
      return prepareStatement(String.format(UPDATE_BLOB_SQL, name, DATA_COL, NAME_COL));
    }
  };
  
  private final ThreadLocal<PreparedStatement> listAllStmt = new ThreadLocal<PreparedStatement>() {

    @Override
    protected PreparedStatement initialValue() {
      return prepareStatement(String.format(GET_ALL_FILES_SQL, NAME_COL, name));    
   }
  };
  
  // Test use only
  public JDBCDirectory(final File dir) throws IOException {
    this(dir.getName(), new Properties(System.getProperties())
      {{
        setProperty(DB_PATH_PROP, dir.getPath());
      }});
  }
  
  public JDBCDirectory(String dirName) throws IOException {
    this(dirName, System.getProperties());
  }
  
  /**
   * @param dirName name identifying this directory, assumed unique among DB table names
   */
  public JDBCDirectory(String dirName, Properties props) throws IOException {
    Connection c = null;
    name = dirName;
    dbDirPath = props.getProperty(DB_PATH_PROP, "~/indexdb");
    dbDirJdbcUrl = props.getProperty(DB_URL_PROP,
        String.format("jdbc:hsqldb:file:%s;shutdown=true", dbDirPath));
    
    try {
      setLockFactory(new SingleInstanceLockFactory());
      dbConn = DriverManager.getConnection(dbDirJdbcUrl);
      
      c = dbConn;
      
      createTableStmt = dbConn.createStatement();
      createTableStmt.setPoolable(true);
      createTableStmt.execute(String.format(CREATE_TABLE_SQL, name, NAME_COL, DATA_COL));
      createTableStmt.close();
      
    } catch (SQLException e) {
      try {
        if (c != null) c.close();
      } catch (SQLException sqle) {}
      throw new IOException(e);
    }

  }

  @Override
  public String[] listAll() throws IOException {
    try (ResultSet allNames = listAllStmt.get().executeQuery()){

      List<String> names = new ArrayList<>();
      
      while (allNames.next()) {
        names.add(allNames.getString(NAME_COL));
      }
      return names.toArray(new String[names.size()]);
      
    } catch (SQLException e) {
      throw new IOException(e);
    } 
  }

  @Override
  public boolean fileExists(String name) throws IOException {
    try (ResultSet rs = getSegmentData(name)){
      return rs.next();
    } catch (SQLException e) {
      throw new IOException(e);
    }
  }

  @Override
  public void deleteFile(String name) throws IOException {
    try {
      PreparedStatement ps = delBlobStmt.get();
      ps.setString(1, name);
      ps.executeUpdate();
    } catch (SQLException e) {
      throw new IOException(e);
    }
  }

  @Override
  public long fileLength(String name) throws IOException {
    Blob b = null;
    try (ResultSet rs = getSegmentData(name)){
      if (!rs.next()) throw new FileNotFoundException(name);
      b = rs.getBlob(DATA_COL);
      return b == null ? 0 : b.length();
    } catch (SQLException e) {
      throw new IOException(e);
    } finally {
      try {
        if (b != null) b.free();
      } catch (SQLException e) {
        throw new IOException(e);
      }
    }
  }

  /**
   * Ordinarily, Lucene never writes twice to the same index file - once the output stream is closed, 
   * the only operations that take place are read and delete. However, there are exceptions.
   * Some directory implementations (e.g. CompoundFileDirectory) do weird things like not closing the 
   * underlying the stream when the close() method is called on managed output. Also, segments.gen can
   * get overwritten. Finally, certain unit tests expect to be able to make intervening writes to the 
   * same file, to emulate index corruption, OR open a stream and then assert on presence of the file 
   * in the directory by calling fileExists() or listAll(). So in order to maintain uniqueness of file 
   * name in the directory via it being the primary key in the table, while supporting all of the above 
   * semantics, we attempt to make interactions with IndexOutput idempotent here. To that end, the 
   * corresponding table row always gets deleted first, then inserted as a placeholder entry. Then, 
   * when the output is closed, it is persisted to the underlying table as an update.   
   * @see org.apache.lucene.store.Directory#createOutput(java.lang.String, org.apache.lucene.store.IOContext)
   */
  @Override
  public IndexOutput createOutput(final String name, IOContext context) throws IOException {
    try {
      try {
        deleteFile(name);
        PreparedStatement ps = addBlobStmt.get();
        ps.setString(1, name);
        ps.executeUpdate();
      } catch (RuntimeException e) {
        // This is a bit hacky but that's what you get with ThreadLocals
        if (e.getCause() instanceof SQLException) throw (SQLException)e.getCause();
        throw e;
      }
      
      final Blob b = dbConn.createBlob();
      
      // No real reason to use {@link OutputStreamIndexOutput} other than laziness; it does all I need, 
      // though adds buffering so we just use a small value here 
      IndexOutput out = new OutputStreamIndexOutput(b.setBinaryStream(1), 128) {
        private boolean isClosed = false;
        
        @Override
        public void close() throws IOException {
          if (isClosed) return;
          // HSQLDB 2.0 specific: first, close the stream in order for data to show up inside the blob. 
          // Then set the blob on the prepared statement, execute it against the DB and close the blob. 
          super.close();
          try {
            PreparedStatement ps = updateBlobStmt.get();
            ps.setBlob(1, b);
            ps.setString(2, name);
            ps.executeUpdate(); 
          } catch (SQLException e) {
            throw new IOException(e);
          } finally {
            try {
              b.free();
            } catch (SQLException e) { }
            isClosed = true;
          }
        }
      };
      return out;
    } catch (SQLNonTransientConnectionException e) {
      throw new AlreadyClosedException("Failed, likely cause is DB connection no longer open", e);
    } catch (SQLException e) {
      throw new IOException(e);
    } 
  }

  @Override
  public void sync(Collection<String> names) throws IOException {
    try {
      // XXX - commit granularity is at connection level which in this case commits all changes on the table, not just those
      // for specified rows
      dbConn.commit();
    } catch (SQLException e) {
      throw new IOException(e);
    }
  }

  @Override
  public IndexInput openInput(final String name, IOContext context) throws IOException {
    try (ResultSet rs = getSegmentData(name)) {
      if (!rs.next()) throw new FileNotFoundException(name);
      Blob b = rs.getBlob(DATA_COL);
      if (b == null) b = dbConn.createBlob();
      return new JDBCIndexInput(name, b);
    } catch (SQLException e) {
      throw new IOException(e);
    }
  }

  @Override
  public void close() throws IOException {
    try {
      dbConn.commit();
      // Shut down db connection
      dbConn.close();
    } catch (SQLException e) {
      throw new IOException(e);
    }
  }
  
  private ResultSet getSegmentData(String segName) throws SQLException {
    PreparedStatement ps = getBlobStmt.get();
    ps.setString(1, segName);
    
    return ps.executeQuery();
  }
  
  private PreparedStatement prepareStatement(String sql) {
    try {
      return dbConn.prepareStatement(sql);
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
  
}
