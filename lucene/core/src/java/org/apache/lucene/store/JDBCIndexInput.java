package org.apache.lucene.store;

import java.io.EOFException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

public class JDBCIndexInput extends IndexInput {

  private long pos = 0;
  private final Blob blob;
  
  // These two are not the same thing. Length can vary depending on whether this object represents a slice of the original input or the whole thing.
  // Limit is constant regardless - it's an optimization to avoid invoking Blob.length() on each read for bound checking
  private final long length;
  private final long readLimit;
  
  public JDBCIndexInput(String name, Blob data) throws SQLException {
    this(name, data, data.length(), data.length());
  }
  
  private JDBCIndexInput(String name, Blob data, long length, long limit) {
    super("JDBCInputStream(name=" + name + ")");
    blob = data;
    this.length = length;
    readLimit = limit;
  }

  @Override
  public void close() throws IOException {
    try {
      blob.free();
    } catch (SQLException e) {
      throw new IOException(e);
    }
  }

  @Override
  public long getFilePointer() {
    return pos;
  }

  @Override
  public void seek(long pos) throws IOException {
    this.pos = pos;
  }

  @Override
  public long length() {
    return length;
  }

  @Override
  public IndexInput slice(String sliceDescription, final long offset, long length) throws IOException {
    if (offset < 0 || length < 0 || offset + length > this.length) {
      throw new IllegalArgumentException("slice() " + sliceDescription + " out of bounds: "  + this);
    }
    final String newResourceDescription = (sliceDescription == null) ? toString() : (toString() + " [slice=" + sliceDescription + "]");
    return new JDBCIndexInput(newResourceDescription, blob, offset + length, readLimit) {
      {
        seek(0L);
      }
      
      @Override
      public void seek(long pos) throws IOException {
        if (pos < 0L) {
          throw new IllegalArgumentException("Seeking to negative position: " + this);
        }
        super.seek(pos + offset);
      }
      
      @Override
      public long getFilePointer() {
        return super.getFilePointer() - offset;
      }

      @Override
      public long length() {
        return super.length() - offset;
      }

      @Override
      public IndexInput slice(String sliceDescription, long ofs, long len) throws IOException {
        return super.slice(sliceDescription, offset + ofs, len);
      }
    };
  }

  @Override
  public byte readByte() throws IOException {
    return readBytes(1)[0];
  }

  @Override
  public void readBytes(byte[] b, int offset, int len) throws IOException {
    byte[] bytes = readBytes(len);
    System.arraycopy(bytes, 0, b, offset, len);
  }
  
  private byte[] readBytes(int len) throws IOException {

    try {
      if (pos + len > readLimit) throw new EOFException();
      byte[] ret = blob.getBytes(pos + 1, len);
      pos += ret.length;
      return ret;
    } catch (SQLException e) {
      throw new IOException(e);
    }
    
  }

}
