package org.apache.lucene.store;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.util.LuceneTestCase.SuppressSysoutChecks;

// Remove some Lucene test framework nuisances:
// - the framework puts a limit on the amount of output to stdout or stderr produced by a test. If it is 
// exceeded, the test fails. Rather than trying to guess the right amount or messing with HSQLDB's 
// logging system, we just disable this check altogether.
@SuppressSysoutChecks(bugUrl="not used")
public class TestJDBCDirectory extends BaseDirectoryTestCase {

  @Override
  protected Directory getDirectory(File path) throws IOException {
    return new JDBCDirectory(path);
  }
  
}
