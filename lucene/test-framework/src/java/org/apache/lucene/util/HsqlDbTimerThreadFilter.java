package org.apache.lucene.util;

import com.carrotsearch.randomizedtesting.ThreadFilter;

public class HsqlDbTimerThreadFilter implements ThreadFilter {

  @Override
  public boolean reject(Thread t) {
      StackTraceElement [] stack = t.getStackTrace();
      if (stack.length > 1 && stack[1].getClassName().equals("org.hsqldb.lib.HsqlTimer$TaskQueue")) {
        return true;
      }
      return false;
  }

}
